#!/bin/sh

p=~/.oh-my-zsh/custom/themes
if [ -f $p/kevin* ];then rm $p/kevin*;fi
ln -s $PWD/kevin.zsh-theme $p/

os=`uname  -a`
if [[ $os =~ "Darwin" ]];then
    echo "mac"
    rm ~/Library/Application\ Support/Code/User/settings.json
    rm -rf ~/Library/Application\ Support/Code/User/snippets
    ln -s $PWD/vscode/snippets      ~/Library/Application\ Support/Code/User/
    ln -s $PWD/vscode/settings.json ~/Library/Application\ Support/Code/User/
elif [[ $os =~ "centos" ]];then
    echo "centos"
elif [[ $os =~ "ubuntu" ]];then
    echo "ubuntu"
    p=~/.config/Code/User
    if [ -d $p/snippets ];then rm -rf $p/snippets; fi
    ln -s $PWD/vscode/snippets $p/

    p=~/.config/Code/User
    if [ -f $p/settings.json ];then rm $p/settings.json; fi
    ln -s $PWD/vscode/settings.json $p/

    p=~/.config/freeplane/1.9.x/templates
    if [ -f $p/kevin-template.mm ];then rm $p/kevin-template.mm; fi
    ln -s $PWD/freeplane/kevin-template.mm $p/ 
else
    echo $os
fi
