<map version="freeplane 1.9.8">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node LOCALIZED_TEXT="new_mindmap" STYLE_REF="k-panton-2020" FOLDED="false" ID="ID_939523375" CREATED="1590130842648" MODIFIED="1605087307445" BORDER_WIDTH="0 px" BORDER_COLOR_LIKE_EDGE="false"><hook NAME="MapStyle" background="#232323">
    <conditional_styles>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" LAST="false">
            <node_level_condition VALUE="1" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" LAST="false">
            <node_level_condition VALUE="0" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" LAST="false">
            <node_level_condition VALUE="2" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
    </conditional_styles>
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff" show_note_icons="true" fit_to_viewport="false" associatedTemplateLocation="file:/home/me/kevin/workspace/klearn/knotes.gitee/Language/rust/rust.mm"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_646396356" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#b4b4b4" STYLE="fork" NUMBERED="false" FORMAT="STANDARD_FORMAT" TEXT_ALIGN="DEFAULT" MAX_WIDTH="20 cm" MIN_WIDTH="0 cm" VGAP_QUANTITY="2 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#808080" BORDER_DASH_LIKE_EDGE="false" BORDER_DASH="SOLID">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#33ff33" WIDTH="1" TRANSPARENCY="200" DASH="3 3" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_646396356" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="Source Han Sans SC Normal" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/html" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/html"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="1" DASH="SOLID"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#b4b4b4" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="k-codestyle" COLOR="#00ff00" BACKGROUND_COLOR="#000000">
<font NAME="Source Han Sans HW SC Regular" SIZE="9"/>
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-txtstyle" COLOR="#ffffff" BACKGROUND_COLOR="#000000">
<font NAME="Source Han Sans HW SC Regular" SIZE="9"/>
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-alarm" COLOR="#ffffff" BACKGROUND_COLOR="#ff0000">
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-panton-2017" COLOR="#000000" BACKGROUND_COLOR="#88b04b">
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-panton-2018" COLOR="#f0f0f0" BACKGROUND_COLOR="#5f4b8b">
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-panton-2019" COLOR="#000000" BACKGROUND_COLOR="#ff6f61">
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-panton-2020" COLOR="#f0f0f0" BACKGROUND_COLOR="#0f4c81" BORDER_WIDTH="0 px" BORDER_COLOR_LIKE_EDGE="false">
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-panton-2021" COLOR="#000000" BACKGROUND_COLOR="#f5df4d"/>
<stylenode TEXT="k-panton-2022-VeryPeri" ID="ID_766458982" COLOR="#ffffff" BACKGROUND_COLOR="#6667ab"/>
<stylenode TEXT="article" COLOR="#cccccc" BACKGROUND_COLOR="#333300">
<edge COLOR="#808080"/>
</stylenode>
<stylenode TEXT="k-panton-2023-Viva-Magenta" BACKGROUND_COLOR="#bb2649"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#0f4c81" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="0 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#0f4c81">
<font SIZE="18" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#cccccc" STYLE="bubble">
<font SIZE="14" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#cccccc">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#cccccc">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#cccccc">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="33" RULE="ON_BRANCH_CREATION"/>
</node>
</map>
