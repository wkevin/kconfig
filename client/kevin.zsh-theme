
function dev-prompt {
  local gover=`go env GOVERSION 2>/dev/null`
  local pyver=`python -V 2>/dev/null`
  local ndver=`node -v 2>/dev/null`
  local proxy=`if [ $http_proxy ];then echo "|p"; fi`
  printf " [%s|%s|%s%s]" ${gover:2:4} ${pyver:7:3} ${ndver:1:5} $proxy;
}

PROMPT="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
PROMPT+='$(dev-prompt) %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
