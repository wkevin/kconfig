#!/bin/sh

# ref:
# https://docs.n8n.io/hosting/environment-variables/environment-variables
docker run -d --name n8n \
    --restart unless-stopped \
    -p 8082:5678 \
    -v ~/n8n/data:/home/node/.n8n \
    -e GENERIC_TIMEZONE="Asia/Shanghai" \
    -e EXECUTIONS_DATA_PRUNE_MAX_COUNT=50 \
    n8nio/n8n

