#!/bin/sh

docker stop nginx8888
docker container prune -f

docker run -d --name nginx8888 \
    -p 80:80 \
    -p 443:443 \
    -v $PWD/conf.d/default:/etc/nginx/conf.d/default:ro \
    -v /home/me/jenkins-data/workspace:/usr/share/nginx/html:ro \
    -e GENERIC_TIMEZONE="Asia/Shanghai" \
    nginx:1.27.3
