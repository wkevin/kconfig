#!/bin/sh

docker stop jupyterhub_py312
docker container prune -f
docker run -d -it --name jupyterhub_py312 -p 8882:8000 -v $PWD:/jupyterhub_config -v $HOME/docker_volumn/jupyterhub:/home jupyterhub:py312

