#!/bin/sh

docker stop jupyterlab_py312
docker container prune -f
docker run -d --name jupyterlab_py312 -p 8882:8888 -v $HOME/docker_volumn/jupyterlab:/usr/src/app jupyterlab:py312

