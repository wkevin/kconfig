#!/bin/sh

docker stop jupyterlab_py311
docker container prune -f
docker run -d --name jupyterlab_py311 -p 8881:8888 -v $HOME/docker_volumn/jupyterlab:/usr/src/app jupyterlab:py311

