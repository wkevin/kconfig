#!/bin/bash

# docker pull plantuml/plantuml-server:latest

# need net.ipv4.ip_forward=1 in /etc/sysctl.conf  ; by cmd: /sbin/sysctl -e -p /etc/sysctl.conf
docker stop plantuml
docker container prune -f

docker run -d --name="plantuml" -p 8082:8080 plantuml/plantuml-server:latest
