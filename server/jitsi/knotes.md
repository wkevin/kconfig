# 更新指南

ref: https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker/

## step1：克隆或拉取最新版本的 docker-jitsi-meet 代码

```SH
cd docker-jitsi-meet
git pull
```

## step2 根据最新的 env.example 文件更新 my.env 文件以确保配置正确

```sh
compare ./env.example ../my.env
```

学习并修改 env，如：

- `JVB_ADVERTISE_IPS=10.57.144.201`: 不配置此 ip 的话，jvb 在局域网内（如：docker）不广播，导致room超过2人就没有 media 流（音频、视频）了。

## step3 在 `$HOME` 下创建 conf 文件夹

```sh
mkdir -p ~/.jitsi-meet-cfg/{web,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}
```

# 启动 docker

```sh
./docker.run-jitsi.sh
```

- 复制 my.env 到 .env，这样可以保证最新版的 my.env 得到应用
- 生成密码，因为 my.env 中没有密码，需要重新生成
- 重启 docker compose
- 复制个性化文件到容器