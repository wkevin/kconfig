#!/bin/bash

# git submodule init


cd docker-jitsi-meet
cp ../my.env ./.env
./gen-passwords.sh

docker-compose stop && docker container prune -f && docker-compose up -d
# docker-compose restart
cd ..

sleep 5
docker cp interface_config.js docker-jitsi-meet-web-1:/config/

docker cp watermark.svg   docker-jitsi-meet-web-1:/usr/share/jitsi-meet/images/
docker cp main-zhCN.json  docker-jitsi-meet-web-1:/usr/share/jitsi-meet/lang/
