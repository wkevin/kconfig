#!/bin/sh

chmod 777 uploads

# docker pull registry.gitlab.com/timvisee/send

docker stop send
docker container prune -f
# example quickstart (point REDIS_HOST to an already-running redis server)
#-e 'REDIS_HOST=localhost' \
docker run -d --name send \
    -p 8086:1443 \
    -v $PWD/uploads:/uploads \
    -e 'DETECT_BASE_URL=true' \
    -e 'FILE_DIR=/uploads' \
    registry.gitlab.com/timvisee/send:latest
