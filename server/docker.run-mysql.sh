#!/bin/bash
docker stop mysql 
docker container prune -f
docker run -d --restart=always --name mysql \
	-e MYSQL_ROOT_PASSWORD=iskevin \
	-p 3366:3306 \
	-v /home/wangjunjian/server.worker/mysql-data/conf:/etc/mysql/conf.d \
	-v /home/wangjunjian/server.worker/mysql-data/logs:/var/log/mysql \
	-v /home/wangjunjian/server.worker/mysql-data/data:/var/lib/mysql \
	-v /home/wangjunjian/server.worker/mysql-data/mysql-files:/var/lib/mysql-files \
        mysql
