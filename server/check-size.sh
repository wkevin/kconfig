#!/bin/sh


df -h

echo '====='

docker system df

echo '====='

find . -maxdepth 1 -type d | xargs -I{} du -sh {} 2>/dev/null | sort -h


echo '====='

cd /
du -sh * 2>/dev/null |sort -h

echo '重启一下 jenkins 好像更能减小空间，jenkins 空间占用待观察'
