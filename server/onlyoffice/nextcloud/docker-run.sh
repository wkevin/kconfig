#!/bin/bash

source .env

if [ ! -d "custom_apps" ]; then mkdir custom_apps; fi
cd custom_apps
if [ ! -d "custom_apps" ]; then mkdir custom_apps; fi

# check onlyoffice folder is exist?
if [ ! -d "custom_apps/onlyoffice" ]; then
  echo "The app 'onlyoffice' does not exist."
  # download an extract it
  wget https://ghproxy.cn/https://github.com/ONLYOFFICE/onlyoffice-nextcloud/releases/download/v${APP_ONLYOFFICE_VER}/onlyoffice.tar.gz
  tar -xzf onlyoffice.tar.gz -C custom_apps/ 
fi

if [ ! -d "custom_apps/drawio" ]; then
  echo "The app 'drawio' does not exist."
  # download an extract it
  wget https://ghproxy.cn/https://github.com/jgraph/drawio-nextcloud/releases/download/v${APP_DRAWIO_VER}/drawio-v${APP_DRAWIO_VER}.tar.gz
  tar -xzf drawio-v${APP_DRAWIO_VER}.tar.gz -C custom_apps/
fi

cd ..

docker-compose stop
docker container prune -f
docker-compose up -d

sleep 10 # owncloud or nextcloud should init itself before ability to enable app

# install app in nextcloud & owncloud:
# before admin created and system inited will fail
# after inted will success

echo "==== config nextcloud ===="
docker exec nxoo-nextcloud-server chown -R www-data:www-data /var/www/html/custom_apps
echo "与 owncloud 不同，nextcloud 需要先在 UI 中执行安装操作，app 才能配置 -- 首次失败后，UI 中完成 install，再次执行 ./docker-run.sh 即可"
docker exec -u www-data nxoo-nextcloud-server php occ app:enable onlyoffice
docker exec -u www-data nxoo-nextcloud-server php occ app:enable drawio
if [ ${USE_HTTPS} == "true" ]; then
docker exec -u www-data nxoo-nextcloud-server php occ config:app:set onlyoffice DocumentServerUrl --value="${BASE_URL}:${OO_HTTPS_PORT}"
else
docker exec -u www-data nxoo-nextcloud-server php occ config:app:set onlyoffice DocumentServerUrl --value="${BASE_URL}:${OO_HTTP_PORT}"
fi
docker exec -u www-data nxoo-nextcloud-server php occ config:app:set onlyoffice jwt_secret --value="${JWT_SECRET}"
docker exec -u www-data nxoo-nextcloud-server php occ config:app:set onlyoffice jwt_header --value="${JWT_HEADER}"

if [ ${USE_HTTPS} == "true" ]; then
##### https: must nextcloud & onlyoffice both enable ssl
docker exec nxoo-nextcloud-server a2enmod ssl
docker exec nxoo-nextcloud-server a2ensite default-ssl
docker exec nxoo-nextcloud-server openssl req -subj "/CN=${BASE_URL}/O=ZTE/C=CN" -new -newkey rsa:2048 -days 3650 -nodes -x509 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem
docker exec nxoo-nextcloud-server service apache2 reload

docker exec nxoo-document-server openssl req -subj "/CN=${BASE_URL}/O=ZTE/C=CN" -new -newkey rsa:2048 -days 3650 -nodes -x509 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem
docker exec nxoo-document-server openssl req -subj "/CN=10.57.144.201/O=ZTE/C=CN" -new -newkey rsa:2048 -days 3650 -nodes -x509 -keyout /etc/ssl/cert-me.key -out /etc/ssl/cert-me.pem
docker exec nxoo-document-server service nginx restart
fi