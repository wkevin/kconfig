#!/bin/bash

cd docker-onlyoffice-owncloud

ln -sf ../.env ./
docker-compose stop
docker container prune -f
docker-compose up -d
