# 2025.1

VDC 迁移，从 ubunt18.04(10.7.225.106) 迁移到 ubutnu20.04(10.57.144.201)

- 原来的 onlyoffice 版本是太低，转存到 2022 目录下
- 新创建 owncloud、nextcloud 两个目录，并分别安装了最新的版本。
- owncloud 完全可用，可用 volume、bind folder —— 当前默认选择了 volume
- nextcloud 分两种
  1. nextcloud(默认自带 apatch)：当前已经调通
  2. nextcloud:fpm（不带 http server，只有 php-fpm）：需要配置 nginx —— 没调通，所以暂时放弃了