#!/bin/bash

source .env

# check onlyoffice folder is exist?
if [ ! -d "onlyoffice" ]; then
  echo "The directory '/onlyoffice' does not exist."
  # download an extract it
  wget https://ghproxy.cn/https://github.com/ONLYOFFICE/onlyoffice-owncloud/releases/download/v${ONLYOFFICE_CONNECTOR_VER}/onlyoffice.tar.gz
  tar -xzf onlyoffice.tar.gz
fi

# restart can not remout volume
# docker-compose restart
docker-compose stop
docker container prune -f
docker-compose up -d

sleep 10 # owncloud or nextcloud should init itself before ability to enable app

# install app in nextcloud & owncloud:
# before admin created and system inited will fail
# after inted will success

echo "==== config owncloud ===="
docker exec -u www-data ocoo-owncloud-server cp -r /tmp/onlyoffice /var/www/owncloud/custom/
docker exec -u www-data ocoo-owncloud-server chown -R www-data:www-data /var/www/owncloud/custom/onlyoffice
docker exec -u www-data ocoo-owncloud-server occ app:enable onlyoffice
docker exec -u www-data ocoo-owncloud-server occ --no-warnings config:app:set onlyoffice DocumentServerUrl --value="${BASE_URL}:${BASE_PORT}/ds-vpath/"
docker exec -u www-data ocoo-owncloud-server occ --no-warnings config:app:set onlyoffice DocumentServerInternalUrl --value="http://onlyoffice-document-server/"
# docker exec -u www-data ocoo-owncloud-server occ --no-warnings config:app:set onlyoffice StorageUrl --value="http://${BASE_URL}:${BASE_PORT}/"
docker exec -u www-data ocoo-owncloud-server occ --no-warnings config:app:set onlyoffice jwt_secret --value="${JWT_SECRET}"
docker exec -u www-data ocoo-owncloud-server occ --no-warnings config:app:set onlyoffice jwt_header --value="${JWT_HEADER}"
