import requests
import time
import json
import logging
import gradio as gr

URL = 'https://gerritpt.zte.com.cn/zte-studio-ai-kbase/openapi/v1/chat'
AUTH_TOKEN = '46b87405b4e04ea68b428ac8a66a278e-544d91062a364ada9a2e3100809cfa48'

class ChatInterface:
    """
    与聊天接口互动的类。

    :param url: API的URL。
    :type url: str
    :param auth_token: API的授权令牌。
    :type auth_token: str
    :param max_length: 格式化后的提示词的最大长度，默认为 2048 字符。
    :type max_length: int
    """

    def __init__(self, url, auth_token, max_length=2048):
        self.url = url
        self.auth_token = auth_token
        self.max_length = max_length

    def format_prompt(self, message, history):
        """
        格式化用户输入的信息和历史信息以生成一个提示词。

        :param message: 本次用户输入的信息。
        :type message: str
        :param history: 包含历史对话的列表，每个元素都是一个包含用户消息和助手回应的元组 (user_message, bot_message).
        :type history: list

        :return: 格式化后的提示词，包括用户和助手的历史对话以及当前用户消息。
        :rtype: str
        """
        prompt = ""
        for user_message, bot_message in history:
            prompt += f"User: {user_message}\nAssistant: {bot_message}\n"
        prompt += f"User: {message}\nAssistant:"

        if self.max_length and len(prompt) > self.max_length:
            # 计算需要去掉的字符数
            chars_to_remove = len(prompt) - self.max_length
            # 去掉字符串开头的部分，只有在需要时才执行切片操作
            if chars_to_remove < len(prompt):
                prompt = prompt[chars_to_remove:]
        return prompt



    def wrap_data(self, prompt):
        """
        包装提示词数据以供请求发送。

        :param prompt: 格式化后的提示词。
        :type prompt: str

        :return: 包装后的数据字典。
        :rtype: dict
        """
        return {
            "chatUuid": "",
            "chatName": "",
            "stream": True,
            "keep": False,
            "text": prompt,
            "model": ""
        }

    def predict(self, message, history):
        """
        使用历史对话和用户消息发送请求并返回响应。

        :param message: 本次用户输入的信息。
        :type message: str
        :param history: 包含历史对话的列表，每个元素都是一个包含用户消息和助手回应的元组 (user_message, bot_message).
        :type history: list

        :return: 从API返回的消息生成器。
        :rtype: str
        """
        headers = {
            'Authorization': f'Bearer {self.auth_token}',
            'Content-Type': 'application/json'
        }

        # 记忆功能
        # prompt = self.format_prompt(message, history)

        # 无记忆
        prompt = message
        data = self.wrap_data(prompt)

        with requests.Session() as session:
            try:
                response = session.post(self.url, headers=headers, json=data, stream=True)
                response.raise_for_status()
                if response.status_code == 200:
                    partial_message = ""
                    for line in response.iter_lines(decode_unicode=True):
                        if line:
                            data_str = line.split("data: ")[1]
                            if data_str == "[DONE]":
                                print("GPT 回答完毕\n")
                                break
                            data = json.loads(data_str)
                            logging.info(data)
                            if 'result' not in data:
                                for w in "知识库内容超过GPT最大长度，请拆分细化当前知识点内容\n":
                                    time.sleep(0.025)
                                    partial_message += w
                                    yield partial_message
                            else:
                                partial_message += data['result']
                                yield partial_message
                else:
                    response.raise_for_status()
            except requests.exceptions.RequestException as err:
                logging.error(f"An error occurred during the request: {err}")
                raise

    def launch_chat_interface(self):
        """
        启动聊天界面，与API进行交互。
        """
        gr.ChatInterface(self.predict,
                examples=["CompBase.name方法怎么用？","TIP_Result.Passed方法怎么用？","请介绍一下pytp.entity.TagMixin.list_api方法"],
                chatbot=gr.Chatbot(height=650),
                title="pyTP 助手",
                description="仅回答 pyTP 相关问题，请自我约束提问内容，共创良好学习氛围。",
        ).queue().launch(share=True, server_name="0.0.0.0")

# 示例用法
if __name__ == "__main__":
    chat_interface = ChatInterface(URL, AUTH_TOKEN)
    chat_interface.launch_chat_interface()
