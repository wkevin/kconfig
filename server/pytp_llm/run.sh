#!/bin/sh

docker stop llm
docker container prune -f
docker run -d --name llm -p 8886:7860 -p 8885:7861 -v $PWD:/usr/src/app pytp_llm
