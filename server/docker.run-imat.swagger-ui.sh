#!/bin/bash

# need net.ipv4.ip_forward=1 in /etc/sysctl.conf  ; by cmd: /sbin/sysctl -e -p /etc/sysctl.conf
docker stop imat-api-ui  imat-api-mock
docker container prune

docker run --name imat-api-ui   -d -p 8888:8080 -e URL=/te/te.yaml -v /home/wangjunjian/jenkins/workspace/imat-engine/:/usr/share/nginx/html/te/ public-docker-virtual.artsz.zte.com.cn/swaggerapi/swagger-ui
docker run --name imat-api-mock -d -p 8090:8000 -v /home/wangjunjian/jenkins/workspace/imat-engine/te.yaml:/te/te.yaml public-docker-virtual.artsz.zte.com.cn/danielgtaylor/apisprout "--watch" /te/te.yaml
