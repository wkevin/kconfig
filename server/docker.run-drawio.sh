#!/bin/bash

# docker pull jgraph/drawio:26.0.4

# need net.ipv4.ip_forward=1 in /etc/sysctl.conf  ; by cmd: /sbin/sysctl -e -p /etc/sysctl.conf
docker stop draw
docker container prune -f

docker run -d --rm --name="draw"  -p 8083:8443 jgraph/drawio:26.0.4

