mkdir zdir
# 进入目录
cd zdir
# 下载最新安装程序
wget https://soft.xiaoz.org/zdir/3.2.0/zdir_3.2.0_linux_amd64.tar.gz
# 解压
tar -xvf zdir_3.2.0_linux_amd64.tar.gz
# 初始化
./zdir init
# 启动
systemctl start zdir
# 停止
systemctl stop zdir
# 重启
systemctl restart zdir
# 开机自启
systemctl enable zdir
