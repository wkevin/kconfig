#!/bin/sh

./init_dir.sh

openssl genrsa -aes256 -out private/rootca.key.pem 2048 
#chmod 400 private/rootca.key.pem
openssl rsa -in private/rootca.key.pem -out private/rootca.key.nopw.pem -passin pass:123456
openssl req -new -config rootca.cnf -sha256 -key private/rootca.key.pem -out csr/rootca.csr.pem
openssl ca -selfsign -config rootca.cnf -in csr/rootca.csr.pem -extensions v3_ca -days 7300 -out certs/rootca.cert.pem
#openssl x509 req -sha1 -signkey private/rootca.key.pem -in csr/rootca.csr.pem -extensions v3_ca -days 7300 -out certs/rootca.cert.pem

