#!/bin/bash

# create dir certs db private crl newcerts under . dir.
if [ -d ./certs ]; then
    rm -rf ./certs
fi
mkdir -p ./certs

if [ -d ./db ]; then
    rm -rf ./db
fi
mkdir -p ./db
touch ./db/index
openssl rand -hex 16 > ./db/serial
echo 1001 > ./db/crlnumber

if [ -d ./private ]; then
    rm -rf ./private
fi
mkdir -p ./private
chmod 700 ./private

if [ -d ./crl ]; then
    rm -rf ./cr.
fi
mkdir -p ./crl

if [ -d ./newcerts ]; then
    rm -rf ./newcerts
fi
mkdir -p ./newcerts

if [ -d ./csr ]; then
    rm -rf ./csr
fi
mkdir -p ./csr

