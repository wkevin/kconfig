#!/bin/sh

docker stop webdav
docker container prune -f

docker run --name webdav \
    -d \
    -v `pwd`/data/:/data/ \
    -p 8083:8883 \
    hacdias/webdav \
    -c /data/config.yml

docker ps -a |grep webdav
docker logs webdav

