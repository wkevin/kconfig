#!/bin/sh

docker stop webdav
docker container prune -f

docker run \
    --name webdav \
    -d \
    --restart always \
    -v `pwd`/data:/var/lib/dav \
    -e AUTH_TYPE=Basic \
    -e USERNAME=wkevin \
    -e PASSWORD=isKevin27 \
    -e SSL_CERT=selfsigned \
    --publish 8083:443 \
    bytemark/webdav

