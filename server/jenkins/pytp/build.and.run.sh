#!/bin/bash

function help() {
    echo "pytp doc jenkins image builder and runner"
    echo "build.and.run.sh [-h] [-b] [-r [tag]]"
    echo "   -h: help"
    echo "   -b: build docker image"
    echo "   -r [tag]: run image, latest when tag is none"
}
if [ $# -eq 0 ];then
help
exit 1
fi

build=NO
run=NO

while getopts "hbr" arg
do
    case $arg in
    h)
      help
      exit;;
    b) 
      build=YES
      shift 1;;
    r)
      run=YES
      shift 1;;
    esac
done

if [ $build == YES ];then
    tag=`date +'%y%m%d%H%M'`
    
    if [ ! -n "$(find app -name pandoc\*)" ];then
    echo "error: need pandoc"
    echo "try: "
    echo "  cd app/"
    echo "  wget http://10.227.45.150:10333/ME/MEData/raw/ed1cc71f0a04e2ffd94875a08370aefa4d0f62d2/app/pandoc-2.7.3-linux.tar.gz"
    exit
    fi
    
    cp ~/.ssh/id_rsa* app/
    echo "docker build -t jbo:$tag ."
    docker build -t jbo:$tag .
    if [ $? -eq 0 ]; then
      docker tag jbo:$tag jbo:latest
      echo "Image build 成功，请配置 Proxy 并安装 blueocean 等插件"
    else
      echo "docker build faild"
    fi
fi

if [ $run == YES ];then
    tag=latest
    if [ $# -eq 1 ];then
    tag=$1
    fi

    if [ ! -n "$(find app -name plantweb)" ];then
    echo "error: need plantweb"
    echo "try: "
    echo "  cd app/"
    echo "  git clone http://10.227.45.150:10333/ME/plantweb.git"
    exit
    fi
    
    docker stop jbo
    docker container prune -f
    docker run -u root --name jbo --rm -d \
      --detach \
      -p 8080:8080 \
      -p 50000:50000 \
      -v $PWD/app:/app \
      -v ~/jenkins-data:/var/jenkins_home \
      -e JAVA_OPTS=-Duser.timezone=Asia/Shanghai \
      -e TZ=Asia/Shanghai \
      jbo:$tag
    docker cp ./app/mdbook-plantuml jbo:/bin/
    docker cp ./app/doxygen-1.10.0/bin/doxygen  jbo:/bin/
    docker exec jbo pip3 install --break-system-packages /app/plantweb/
    docker exec jbo npm install -g @marp-team/marp-cli
    docker ps -a
    
fi
