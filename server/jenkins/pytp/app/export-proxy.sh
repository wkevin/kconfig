#!/bin/bash

export http_proxy=http://$1:$2
export https_proxy=http://$1:$2
export ftp_proxy=http://$1:$2
export all_proxy=http://$1:$2
export HTTP_PROXY=http://$1:$2
export HTTPS_PROXY=http://$1:$2
export FTP_PROXY=http://$1:$2
export ALL_PROXY=http://$1:$2

export no_proxy=*.zte.com.cn,.zte.com.cn,10.0.0.0/8
export NO_PROXY=*.zte.com.cn,.zte.com.cn,10.0.0.0/8

env |grep proxy
env |grep PROXY
