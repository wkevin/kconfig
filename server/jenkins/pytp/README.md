请在 app/ 放置 pandoc-2.7.3-linux.tar.gz

## 如何配置 docker container 中的 proxy，使其访问外网

1. 配置 225.106-host 的 proxy，方法有几种:
  1. 在云桌面上：`ssh -R 0.0.0.0:6789:proxysz.zte.com.cn:80 wangjunjian@10.7.225.106`
  2. 在测试机上：`ssh -R 0.0.0.0:6789:127.0.0.1:6789 wangjunjian@10.7.225.106`
2. docker run 使用 host 网络模式：`docker run --network="host" ...`
  - 此时 `-p 8080:8008` 之类的端口映射会失效，直接 port 暴露在 host 上
3. 配置 docker 的 proxy
  1. 网页上配置：Manage Jenkins - System - HTTP Proxy Configuration
    - Server：配置为 127.0.0.1， 不能加 http —— 这个害死我了，整了好几个小时才发现，那 socks5 协议的咋整？
    - Port：6789
  2. 进入 docker: `docker exec -it jbo bin/bash`
    - 容器中 `hostname -i` 得到与 host 一样的 ip: 127.0.0.1
    - 配置 `export http_proxy=http://127.0.0.1:6789` & ...
      - `docker run -e http_proxy=...` 可以达到同样效果
      - 修改 $host/ ~/.docker/config.ini 文件也可以达到同样效果

比较重要的 plugin 有：

- blueocean：安装后才有 pipeline、multibranch pipeline 等
- webhook
- ssh

**正常编译不能配置 proxy，否则无法从 gerrit、制品库获取数据，请注意去掉 Proxy。**