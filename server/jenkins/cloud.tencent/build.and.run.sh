#!/bin/bash

function help() {
    echo "cloud tencent jenkins image builder and runner"
    echo "build.sh [-h] [-b] [-r] [-d]"
    echo "   -h: help"
    echo "   -b: build docker image and run it"
    echo "   -r: run lastest image"
    echo "   -d: delete docker images"
}
if [ $# -eq 0 ];then
help
exit 1
fi

build=NO
run=NO
del=NO
while getopts "hbrd" arg
do
    case $arg in
    h)
        help
        exit;;
    b) 
	build=YES
	shift 1;;
    d) 
	del=YES
	shift 1;;
    r)
	run=YES
	shift 1;;
    esac
done

if [ $build == YES ];then
    tag=`date +'%y%m%d%H%M'`
    
    if [ ! -n "$(find app -name pandoc\*)" ];then
    echo "error: need pandoc"
    echo "try: "
    echo "  cd app/"
    echo "  wget https://github.com/jgm/pandoc/releases/download/2.14.2/pandoc-2.14.2-linux-amd64.tar.gz"
    exit
    fi
    
    cp ~/.ssh/id_rsa* app/
    echo "docker build -t jbo:$tag ."
    docker build -t jbo:$tag .
    docker tag jbo:$tag jbo:latest
fi

if [ $run == YES ];then
    docker stop jbo
    docker container prune -f
    # -u root: deal with "java.nio.file.AccessDeniedExeption" error
    docker run -u root --name jbo --rm -d \
      --shm-size=1gb \
      -p 8088:8080 \
      -p 50000:50000 \
      -v $PWD/app:/app \
      -v /home/ubuntu/server-data/jenkins_home:/var/jenkins_home \
      -e JAVA_OPTS=-DusER.TImezone=Asia/Shanghai \
      -e TZ=Asia/Shanghai \
      jbo:latest
    docker ps -a
    
fi

if [ $del == YES ];then
    docker images | awk '/^jbo/{print $3}' |xargs -I{} docker rmi {} 
    docker images | awk '/^<none/{print $3}' |xargs -I{} docker rmi {} 
    docker images
fi
