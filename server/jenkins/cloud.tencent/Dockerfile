FROM mirror.ccs.tencentyun.com/jenkinsci/blueocean:1.24.8

USER root

# APK
RUN rm /etc/apk/repositories
RUN echo "https://mirrors.ustc.edu.cn/alpine/v3.14/main" > /etc/apk/repositories
RUN echo "https://mirrors.ustc.edu.cn/alpine/v3.14/community" >> /etc/apk/repositories
RUN mv    /usr/glibc-compat/lib/ld-linux-x86-64.so.2 /usr/glibc-compat/lib/ld-linux-x86-64.so
RUN ln -s /usr/glibc-compat/lib/ld-linux-x86-64.so   /usr/glibc-compat/lib/ld-linux-x86-64.so.2
RUN apk upgrade

# Toolchain
RUN apk add --no-cache gcc g++ make cmake glibc 

# python
RUN apk add --no-cache libffi-dev openssl-dev zlib-dev
RUN apk add --no-cache python3 python3-dev py3-pip py3-setuptools py3-wheel
RUN ln -s `which python3` /bin/python && python -V && pip -V
RUN pip config set global.index-url "https://mirrors.ustc.edu.cn/pypi/web/simple"

# node.js
RUN apk add nodejs npm
RUN npm config set registry https://registry.npm.taobao.org

WORKDIR /app
COPY app/* /app/

# Pandoc
RUN tar xvf /app/pandoc-*-linux-amd64.tar.gz --strip-components 1 -C /usr/local/

RUN npm install -g @marp-team/marp-cli
RUN pip install httplib2
RUN apk add --no-cache \
      chromium \ 
      ghostscript \
      nss \
      freetype \
      harfbuzz \
      ca-certificates \
      ttf-freefont \
      yarn

# Add user so we don't need --no-sandbox.
#RUN addgroup -S pptruser && adduser -S -g pptruser pptruser \
#    && mkdir -p /home/pptruser/Downloads /app \
#    && chown -R pptruser:pptruser /home/pptruser \
#    && chown -R pptruser:pptruser /app \
#    && chown -R pptruser:pptruser /var/jenkins_home

# Run everything after as non-privileged user.
#USER pptruser

USER jenkins

WORKDIR /var/jenkins_home/workspace/
