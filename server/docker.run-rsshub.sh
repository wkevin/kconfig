#!/bin/sh

docker stop rsshub
docker container prune -f
docker run -d --name rsshub -p 8081:1200 diygod/rsshub
