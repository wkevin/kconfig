# ABOR - abort a file transfer
# CWD - change working directory
# DELE - delete a remote file
# LIST - list remote files
# MDTM - return the modification time of a file
# MKD - make a remote directory
# NLST - name list of remote directory
# PASS - send password
# PASV - enter passive mode
# PORT - open a data port
# PWD - print working directory
# QUIT - terminate the connection
# RETR - retrieve a remote file
# RMD - remove a remote directory
# RNFR - rename from
# RNTO - rename to
# SITE - site-specific commands
# SIZE - return the size of a file
# STOR - store a file on the remote host
# TYPE - set transfer type
# USER - send username


# ACCT* - send account information
# APPE - append to a remote file
# CDUP - CWD to the parent of the current directory
# HELP - return help on using the server
# MODE - set transfer mode
# NOOP - do nothing
# REIN* - reinitialize the connection
# STAT - return server status
# STOU - store a file uniquely
# STRU - set file transfer structure
# SYST - return system type

CWD - change working directory 更改目录
LIST - list remote files 列目录
MKD - make a remote directory 新建文件夹
NLST - name list of remote directory
PWD - print working directory 显示当前工作目录
RETR - retrieve a remote file 下载文件
STOR - store a file on the remote host 上传文件


DELE - delete a remote file 删除文件
RMD - remove a remote directory 删除目录
RNFR - rename from 重命名
RNTO - rename to 重命名


# ABOR - abort a file transfer 取消文件传输
# CWD - change working directory 更改目录
# DELE - delete a remote file 删除文件
# LIST - list remote files 列目录
# MDTM - return the modification time of a file 返回文件的更新时间
# MKD - make a remote directory 新建文件夹
# NLST - name list of remote directory
# PASS - send password
# PASV - enter passive mode
# PORT - open a data port 打开一个传输端口
# PWD - print working directory 显示当前工作目录
# QUIT - terminate the connection 退出
# RETR - retrieve a remote file 下载文件
# RMD - remove a remote directory
# RNFR - rename from
# RNTO - rename to
# SITE - site-specific commands
# SIZE - return the size of a file 返回文件大小
# STOR - store a file on the remote host 上传文件
# TYPE - set transfer type
# USER - send username

# less common commands:

# ACCT* - send account information
# APPE - append to a remote file
# CDUP - CWD to the parent of the current directory
# HELP - return help on using the server
# MODE - set transfer mode
# NOOP - do nothing
# REIN* - reinitialize the connection
# STAT - return server status
# STOU - store a file uniquely
# STRU - set file transfer structure
# SYST - return system type

 

