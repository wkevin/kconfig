vsftp
===========================================================================
重点步骤：
1.用户输入的用户名和密码
2.进行PAM验证：本地用户和虚拟用户可以同时使用一个pam文件进行验证
3.本地用户映射：guest_enable=YES：所有非匿名登录的用户都映射为 guest_username=*** 的用户，使用 *** 用户的权限，默认到该用户的主目录上
                                  问题：其他本地用户也将会被指向 *** 的主目录，而不是自己的主目录——很遗憾的是貌似这个不好解决
                guest_enable=NO（默认）：本地用户指向各自的主目录，虚拟用户则被deny
                
===========================================================================

/etc/rc.d/init.d/vsftpd          启动脚本 
/usr/sbin/vsftpd                 Vsftpd的主程序 
/etc/vsftpd/vsftpd.conf          主配置文件 
/etc/pam.d/vsftpd                PAM认证文件（此文件中file=/etc/vsftpd/ftpusers字段，指明阻止访问的用户来自/etc/vsftpd/ftpusers文件中的用户） 
/etc/vsftpd/ftpusers             禁止使用vsftpd的用户列表文件。记录不允许访问FTP服务器的用户名单，管理员可以把一些对系统安全有威胁的用户账号记录在此文件中，以免用户从FTP登录后获得大于上传下载操作的权利，而对系统造成损坏。（注意：linux-4中此文件在/etc/目录下）
/etc/vsftpd/user_list            禁止或允许使用vsftpd的用户列表文件。这个文件中指定的用户缺省情况（即在/etc/vsftpd/vsftpd.conf中设置userlist_deny=YES）下也不能访问FTP服务器，在设置了userlist_deny=NO时,仅允许user_list中指定的用户访问FTP服务器。（注意：linux-4中此文件在/etc/目录下）
/var/ftp                         匿名用户主目录；本地用户主目录为：/home/用户主目录，即登录后进入自己家目录 
/var/ftp/pub                     匿名用户的下载目录，此目录需赋权根chmod 1777 pub（1为特殊权限，使上载后无法删除） 
/etc/logrotate.d/vsftpd.log      Vsftpd的日志文件
 
===========================================================================

史上最详细的vsftpd配置文件讲解: http://os.51cto.com/art/201008/221842.htm
chroot_local_user=YES       #是否将所有用户锁定在主目录,YES为启用 NO禁用.(包括注释掉也为禁用)
chroot_list_enable=YES     #是否启动锁定用户的名单 YES为启用  NO禁用(包括注释掉也为禁用)
chroot_list_file=/etc/vsftpd.chroot_list     #禁用的列表名单  格式为一行一个用户, 如果名单里面有一个ftpuser的用户, 则ftpuser用户不会锁定在主目录,用户将可以自由切换目录.

===========================================================================



