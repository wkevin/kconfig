# Apache配置

Apache软件基金会（也就是Apache Software Foundation，简称为ASF），正式创建于1999年
早期的apache小组，现在已经成为一个拥有巨大力量的apache基金会。
他们把起家的apache更名为httpd（apache和httpd是一个，到2后就叫httpd了。 ），也更符合其http server的特性。而apache现在成为 apache基金会下几十种开源项目的标识。

## Oops
yum 失败：
/tmp 空间不足
/var 空间不足：yum 的cache和tmp 都在这里，直接删除即可，因为在某个user下 yum clean 只会clean这个用户的。

## 安装 apache、php

```
yum install httpd php

# httpd -v
Server version: Apache/2.2.15 (Unix)
Server built:   Apr  4 2010 17:15:21

安装apache扩展
yum install httpd-manual mod_ssl mod_perl mod_auth_mysql 

安装php的扩展
yum install php-gd php-xml php-mbstring php-ldap php-pear php-xmlrpc 

配置开机启动服务
chkconfig httpd on 

启动httpd服务
service httpd start

配置防火墙（iptables）：
添加允许访问HTTP、FTP端口
service iptables restart 
```


## 配置 apache

配置出来的默认站点目录为/var/www/html/ 新建一个测试php脚本:
```
<?php
phpinfo();
?> 
```
同时可以查看 httpd 使用的用户/组： User/Group： apache/apache

编辑apache的http.conf
```
httpd-2.2.15-1.fc13.x86_64 : Apache HTTP Server
Repo        : fedora
```
匹配来自于:
Filename    : /etc/httpd/conf/httpd.conf
修改 DocumentRoot

## 网页跳转

1. body onload跳转法
```
<body onload=“parent.location=‘http://www.ieseo.net/’”> 这种方法也能够被搜索引擎识别。
```
2. meta refresh跳转法
```
<meta http-equiv="refresh" content="1;url=/wiki/index.php">
上述html代码中的“10”是延时跳转的时间，单位是秒。如果设为0，就表示立即跳转。由于搜索引擎能够读取html，所以对于这种自动跳转方法，搜索引擎是能够自动检测出来的。 如果跳转时间为0，就可能会被视为作弊，从而受到惩罚。如果有时间延迟（一般10秒以上），就会被视为正常应用。
```
3. 表单跳转法
```
<form name=“form1” action=http://www.ieseo.net/default.asp method=“get”> </form> <script language=“javascript”> document.form1.submit（） </script> 其中form1名称任意，但二处form1应该统一。action中的url地址必须以文件名结尾，例如“action=http://www.ieseo.net /”或“action=http://www.ieseo.net/post/”便是不规范写法。由于搜索引擎的蜘蛛程序是不会填写表单的，所以搜索引擎便无法识别通过这种方法实现的网页自动跳转。
```
4. js跳转法
```
<script language=“javascript”> location.replace（http://www.ieseo.net） </script> 其中的“http://www.ieseo.net”是重定向目标地址。 由于搜索引擎无法解析javascript，所以搜索引擎便无法识别用javascript脚本进行的自动跳转。
```
5. 程序跳转法（response.redirect 、server.transfer）
```
response.redirect和server.transfer都能实现网页自动跳转，前者可以跳转到站内url，也可以跳转到站外url，而后者只能跳转到站内url，且浏览器地址栏的url将保持不变。
```
